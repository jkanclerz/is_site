import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-science-group',
  templateUrl: './science-group.component.html',
  styleUrls: ['../../assets/main.css',
  './science-group.component.css'
  ]
})
export class ScienceGroupComponent implements OnInit {
  activeGroupId:number;
  constructor() {
    this.activeGroupId =2;
   }

  ngOnInit() {
  }

  // let groups : Array[any];
  groups: { id: number, name: string, description: string, logoUrl: string }[] = [
  {
    id: 1,
    name: "Koło Naukowe Informatyki",
    description: "Koło Naukowe Informatyki jest jednym z najstarszych kół naukowych na Naszej uczelni. Od 1976 roku zrzesza ono studentów, którzy chcą rozwijać swoje umiejętności z zakresu informatyki, a także którzy chcą brać udział w ciekawych wydarzeniach i spotkaniach. KNI może pochwalić się zdobyciem Złotego Dysku Elastycznego w 1985 roku, nagrody dla najlepszego Koła Naukowego Informatyki w Polsce. Warto wspomnieć, że od 2005 roku KNI wraz z trzema innymi Kołami organizuje Studencki Festiwal Informatyczny, który co roku gości znane osobistości z branży IT, daje możliwość wzięcia udziału w różnego rodzaju warsztatach i wykładach.",
    logoUrl: '../../assets/images/kni_logo.png'
  },
  {
    id: 2,
    name: "Koło Naukowe >DEV",
    description: "Koło naukowe >dev jest to organizacja studencka zrzeszająca pasjonatów programowania i informatyki sprzętowej. Wspólnie tworzymy ciekawe systemy jak i oprogramowania do zarządzania nimi. Czasami bierzemy też śrubokręt, młotek lub lutownice do ręki i tworzymy coś fizycznego, namacalnego. Zazwyczaj co tydzień (codziennie) spotykamy się w pokoju 426 aby porozmawiać o bieżących sprawach i na tematy które czasem odbiegają dalece od informatyki. Przyjdź i sprawdź !",
    logoUrl: '../../assets/images/dev.png'
  }];


  setActiveGroup(setGroupId) {
    this.activeGroupId =setGroupId;
    console.log(this.activeGroupId);
  }


}
