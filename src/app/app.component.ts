import { Component } from '@angular/core';
import { MnFullpageService } from 'ng2-fullpage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', '../assets/main.css', '../../node_modules/bootstrap/dist/css/bootstrap.min.css']
})
export class AppComponent {
  title = 'app works!';
  constructor(private fullpageService: MnFullpageService){

  }
}
