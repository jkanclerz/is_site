import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: [
    '../../assets/main.css',
    './reviews.component.css',
    '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
  ]
})

export class ReviewsComponent implements OnInit {
  startScope: number;
  reviews: ReviewModel[];
  constructor() {
    this.startScope = 0;
    this.reviews = this.filterMsg(this.startScope);
  }

  ngOnInit() { }

  reviewInit: ReviewModel[] = [
    // {
    //   id: 0,
    //   review: "Tutaj moze byc opisutaj moze yc opisutaj moze byc opisutaj moze byc opyc opisutaj moze byc opisutaj moze byc opyc opisutaj moze byc opisutaj moze byc opisutaj moze byc opis",
    //   author: "BB TEST"
    // },
    // {
    //   id: 1,
    //   review: "Tutaj moze byc opisutaj moze yc opisutaj moze byc opisutaj moze byc opyc opisutaj moze byc opisutaj moze byc opyc opisutaj moze byc opisutaj moze byc opisutaj moze byc opis",
    //   author: "BB TEST2"
    // },
    // {
    //   id: 2,
    //   review: "Tutaj moze byc opisutaj moze yc opisutaj moze byc opisutaj moze byc opyc opisutaj moze byc opisutaj moze byc opyc opisutaj moze byc opisutaj moze byc opisutaj moze byc opis",
    //   author: "BB TEST3"
    // },
    // {
    //   id: 3,
    //   review: "Tutaj moze byc opisutaj moze yc opisutaj moze byc opisutaj moze byc opyc opisutaj moze byc opisutaj moze byc opyc opisutaj moze byc opisutaj moze byc opisutaj moze byc opis",
    //   author: "BB TEST4"
    // },
    // {
    //   id: 4,
    //   review: "Tutaj moze byc opisutaj moze yc opisutaj moze byc opisutaj moze byc opyc opisutaj moze byc opisutaj moze byc opyc opisutaj moze byc opisutaj moze byc opisutaj moze byc opis",
    //   author: "BB TEST5"
    // }
    ];



  filterMsg(scope: number) {
    return this.reviewInit.filter(function (el) {
      return el.id >= scope && el.id <= scope + 2;
    });
  }

  sliderGo(direction: number) {
    this.startScope += direction;
    if (this.startScope >= this.reviewInit.length) {
      this.startScope = 0;
    }
    if (this.startScope < 0) {
      this.startScope = this.reviewInit.length - 1;
    }
    this.reviews = this.filterMsg(this.startScope);
  }
}


export class ReviewModel{
  id: number;
  review: string;
  author: string;
}
