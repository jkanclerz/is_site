import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graduate',
  templateUrl: './graduate.component.html',
  styleUrls: [
    '../../assets/main.css',
    './graduate.component.css'
    ]
})
export class GraduateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
