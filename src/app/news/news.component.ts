import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['../../assets/main.css',
    './news.component.css', 
    '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
  ]
})
export class NewsComponent implements OnInit {

startScope: number;
messages: { id: number, title: string, text: string }[];
  constructor() {
       this.startScope = 0;
       this.messages = this.filterMsg(this.startScope);
  }

  ngOnInit() {}

      messagesInit: { id: number, title: string, text: string }[] = [
      // {
      //   id: 0,
      //   title: "1)Tytuł to je",
      //   text: "Tutaj moze byc opisutaj moze yc opisutaj moze byc opisutaj moze byc opyc opisutaj moze byc opisutaj moze byc opyc opisutaj moze byc opisutaj moze byc opisutaj moze byc opis",
      // },
      // {
      //   id: 1,
      //   title: "2Ptaki latają kluczem",
      //   text: "Tutaj mozesadsadsadsa zesaesadsadsadsa zesadsadsadsa zeesadsadsadsa zesadsadsadsa zeesadsadsadsa zesadsadsadsa zedsadsadsa zesadsadsadsa zesadsadsadsa ds vdsfds fsd",
      // },
      // {
      //   id: 2,
      //   title: "3Ptaki latają kluczem",
      //   text: "Tutaj mozesadsadsadsasadsadsadsa ds vozesadsadsadsa dsadsadsadsa ds vozesadsadsadsa dsadsadsadsa ds vozesadsadsadsa d ds vozesadsadsadsa ds vozesadsadsadsa ds vozesadsadsadsa ds vdsfds fsd",
      // },
      // {
      //   id: 3,
      //   title: "4Ptaki latają kluczem",
      //   text: "Tutaj mozesadsadj mozesadsadj adsadj mozesadsadj mozadsadj mozesadsadj mozadsadj mozesadsadj mozadsadj mozesadsadj mozmozesadsadj mozesadsadsadsa ds vdsfds fsd",
      // },
      // {
      //   id: 4,
      //   title: "5Ptaki latają kluczem",
      //   text: "Tutaj mozesadsadmozesadsamozesadsa mozesadsaozesadsadmozesadsamozesa dsamozesaozesadsadmozesadsamozesadsamozesaozesad  sadmozesadsamozesadsamozesamozesadsasadsa ds vdsfds fsd",
      // }
      ];



      filterMsg(scope: number){
          return this.messagesInit.filter(function (el){
             return el.id >= scope && el.id <= scope+1;
          });
      }

      sliderGo(direction: number){
          this.startScope += direction;
          if(this.startScope >= this.messagesInit.length){
              this.startScope = 0;
          }
          if(this.startScope < 0){
              this.startScope = this.messagesInit.length-1;
          }
          this.messages = this.filterMsg(this.startScope);
      }
}
