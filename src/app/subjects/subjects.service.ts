import { Injectable, Inject, Optional } from '@angular/core';

@Injectable()
export class SubjectsService {

  constructor(@Optional() @Inject('GeneralSubjects') generalSubjects,
              @Optional() @Inject('CoreSubjectBachelorDegree') coreSubjectBachelorDegree,
              @Optional() @Inject('CoreSubjectMasterDegree') coreSubjectMasterDegree,
              @Optional() @Inject('GeneralStudyData') generalStudyData) {
    this.generalSubjects = generalSubjects;
    this.coreSubjectBachelorDegree = coreSubjectBachelorDegree;
    this.coreSubjectMasterDegree = coreSubjectMasterDegree;
    this.generalStudyData = generalStudyData;
  }

  generalSubjects = [];
  coreSubjectBachelorDegree = [];
  coreSubjectMasterDegree = [];
  generalStudyData = [];

  getGeneralSubjects(){
    return this.generalSubjects;
  }

  getCoreSubjectBachelorDegree(){
    return this.coreSubjectBachelorDegree;
  }

  getCoreSubjectMasterDegree(){
    return this.coreSubjectMasterDegree;
  }

  getGeneralStudyData(){
    return this.generalStudyData;
  }
}
