import { Component, OnInit } from '@angular/core';
import { SubjectsService } from '../subjects/subjects.service'

@Component({
  selector: 'app-study-programme',
  templateUrl: './study-programme.component.html',
  styleUrls: [
    './study-programme.component.css',
    '../../assets/main.css',
    '../subjects/subjects.component.css'
  ]
})
export class StudyProgrammeComponent implements OnInit {

  activeGroupId: number;
  activeSpecId: number;
  activeGroupClass: string;
  activeSpecClass: string;

  constructor(private subjectsService:SubjectsService) {
    this.activeGroupId = 2;
    this.activeSpecId = 0;
    this.activeGroupClass = 'bachelor';
    this.activeSpecClass = 'iz1';
  }

  ngOnInit() {
    this.generalSubjects = this.subjectsService.getGeneralSubjects();
    this.coreSubjectBachelorDegree = this.subjectsService.getCoreSubjectBachelorDegree();
    this.coreSubjectMasterDegree = this.subjectsService.getCoreSubjectMasterDegree();
    this.generalStudyData = this.subjectsService.getGeneralStudyData();
  }
  generalSubjects = [];
  coreSubjectBachelorDegree = [];
  coreSubjectMasterDegree = [];
  generalStudyData = [];


  setActiveGroup(e, setGroupId, activeClass) {
    if (activeClass === 'bachelor') {
      this.activeGroupClass = 'bachelor'
    }
    if (activeClass === 'master') {
      this.activeGroupClass = 'master'
    }
    this.activeGroupId = setGroupId;
  }

  setActiveSpec(e, setGroupId, activeClass) {
    if (activeClass === 'iz1') {
      this.activeSpecClass = 'iz1'
    }
    if (activeClass === 'sin1') {
      this.activeSpecClass = 'sin1'
    }
    if (activeClass === 'sit2') {
      this.activeSpecClass = 'sit2'
    }
    if (activeClass === 'sin2') {
      this.activeSpecClass = 'sin2'
    }
    this.activeSpecId = setGroupId;
  }

  setGroupAndSpec(e) {
    this.setActiveGroup(e, e.groupId, e.activeGroupClass);
    this.setActiveSpec(e, e.specId, e.activeSpecClass);
  }

}
