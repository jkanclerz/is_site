import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MnFullpageDirective, MnFullpageService } from "ng2-fullpage";

import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { NewsComponent } from './news/news.component';
import { RecruitmentComponent } from './recruitment/recruitment.component';
import { ContactComponent } from './contact/contact.component';
import { MenuComponent } from './menu/menu.component';
import { AlertModule } from 'ng2-bootstrap';
import { ScienceGroupComponent } from './science-group/science-group.component';

import { CarouselModule } from 'ng2-bootstrap';
import { GraduateComponent } from './graduate/graduate.component';
import { ModalComponent } from './modal/modal.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { SpecializationComponent } from './others/specialization.component';
import { DegreeComponent } from './others/degree.component';

import { SubjectsService } from './subjects/subjects.service'

import generalSubjects from './data/generalSubjects.data'
import coreSubjectBachelorDegree from './data/coreSubjectBachelorDegree.data'
import coreSubjectMasterDegree from './data/coreSubjectMasterDegree.data'
import generalStudyData from './data/generalStudyData.data';
import { StudyProgrammeComponent } from './study-programme/study-programme.component'

@NgModule({
  declarations: [
    AppComponent,
    ReviewsComponent,
    HomepageComponent,
    NewsComponent,
    RecruitmentComponent,
    ContactComponent,
    ScienceGroupComponent,
    MenuComponent,
    GraduateComponent,
    ModalComponent,
    SubjectsComponent,
    SpecializationComponent,
    DegreeComponent,
    MnFullpageDirective,
    StudyProgrammeComponent // add MnFullpageDirective declaration here
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AlertModule.forRoot(),
    CarouselModule.forRoot(),
  ],
  providers: [
    MnFullpageService, // also add MnFullpageService provider here
    SubjectsService,
    { provide: 'GeneralSubjects', useValue: generalSubjects},
    { provide: 'CoreSubjectBachelorDegree', useValue: coreSubjectBachelorDegree},
    { provide: 'CoreSubjectMasterDegree', useValue: coreSubjectMasterDegree},
    { provide: 'GeneralStudyData', useValue: generalStudyData}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
