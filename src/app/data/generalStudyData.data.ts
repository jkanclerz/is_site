/**
 * Created by cburas on 23.03.2017.
 */
export default [
  {
    id: 1,
    specialization: [
      {
      spec_id: 0,
      name: "inzynieria oprogramowania",
      description: "Na tej specjalności studenci oprócz języków programowania (m.in. C, C++, Visual C++, C#, Java, JavaScript, PHP) poznają zasady projektowania programów dopasowanych do wymagań użytkowników. Celem specjalności jest kształcenie w zakresie tworzenia i wykorzystywania programów komputerowych, w tym również aplikacji internetowych i mobilnych. Absolwenci będą mogli ubiegać się o pracę głównie na stanowiskach związanych z tworzeniem programów komputerowych, projektowaniem, uruchamianiem i konserwacją sieci komputerowych, zastosowaniem Internetu w biznesie (e-biznes) czy administrowaniem systemami informatycznymi i sieciami komputerowymi.",
      subjects: [
        {
          name: "Administrowanie systemami komputerowymi",
          description: "Administrowanie systemami komputerowymi"
        },
        {
          name: "Metodyka Pracy Naukowej",
          description: "Metodyka Pracy Naukowej"
        },
        {
          name: "Zarządzanie przedsięwzięciami informatycznymi",
          description: "Zarządzanie przedsięwzięciami informatycznymi"
        },
        {
          name: "Analiza i projektowanie obiektowe",
          description: "Analiza i projektowanie obiektowe"
        },
        {
          name: "Współczesne trendy informatyki",
          description: "Współczesne trendy informatyki"
        },
        {
          name: "Programowanie współbieżne i równoległe",
          description: "Programowanie współbieżne i równoległe"
        },
        {
          name: "Programowanie systemów mobilnych",
          description: "Programowanie systemów mobilnych"
        },
        {
          name: "Programowanie systemów rozproszonych",
          description: "Programowanie systemów rozproszonych"
        },
        {
          name: "Systemy zintegrowane",
          description: "Systemy zintegrowane"
        },
        {
          name: "",
          description: ""
        }
      ]
    },
    {
      spec_id: 1,
      name: "systemy informacyjne",
      description: "Informatyka to nie tylko programowanie. Dzisiaj pojedyncze programy komputerowe są częścią większych systemów informatycznych, podporządkowanych celom organizacji z nich korzystających.  Zanim powstanie program komputerowy musi on być zaprojektowany zgodnie z wymaganiami użytkowników – jest to zadanie dla analityków i projektantów, którzy posiadają nie tylko wiedzę techniczną, ale również rozumieją miejsce i rolę programu komputerowego w organizacji.  Celem specjalności „Systemy Informacyjne” jest przygotowanie studentów do aktywnego udziału w tworzeniu i eksploatacji nowych systemów informatycznych, poprzez dostarczenie im nie tylko wiedzy z zakresu informatyki, ale również wiedzy z zakresu zarządzania i ekonomii.",
      subjects: [
        {
          name: "Audyt i kontrola SI",
          description: "Audyt i kontrola SI"
        },
        {
          name: "Metodyka Pracy Naukowe",
          description: "Metodyka Pracy Naukowe"
        },
        {
          name: "Zarządzanie infrastrukturą Informatyczną",
          description: "Zarządzanie infrastrukturą Informatyczną"
        },
        {
          name: "Bezpieczeństwo SI",
          description: "Bezpieczeństwo SI"
        },
        {
          name: "Zarządzanie przedsięwzięciami informatycznymi",
          description: "Zarządzanie przedsięwzięciami informatycznymi"
        },
        {
          name: "Systemy zintegrowane",
          description: "Systemy zintegrowane"
        },
        {
          name: "Systemy wspomagania decyzji",
          description: "Systemy wspomagania decyzji"
        },
        {
          name: "Zarządzanie wiedzą",
          description: "Zarządzanie wiedzą"
        },
        {
          name: "Metody eksploracji danych",
          description: "Metody eksploracji danych"
        },
        {
          name: "Zarządzanie przedsięwzięciami informatycznymi ",
          description: "Zarządzanie przedsięwzięciami informatycznymi "
        },
        {
          name: "Komunikacja w zepole projektowym ",
          description: "Komunikacja w zepole projektowym "
        },
        {
          name: "",
          description: ""
        }
      ]
    },
      {
        spec_id: 2,
        name: "systemy inteligentne",
        description: "Studenci specjalności poznają w trakcie swoich studiów podstawy teoretyczne typów systemów inteligentnych takich jak: systemy eksploracyjnej analizy danych, systemy uczące się, systemy wspomagające procesy decyzyjne, systemy automatycznego przetwarzania języka naturalnego, systemy ekspertowe i systemy rozmyte. Zdobywają też wiedzę i umiejętności niezbędne do samodzielnego tworzenia tego typu rozwiązań oraz ich wdrażania w obszarze ekonomii i zarządzania.",
        subjects: [
          {
            name: "Systemy uczące się",
            description: "Systemy uczące się"
          },
          {
            name: "Systemy rozmyte",
            description: "Systemy rozmyte"
          },
          // {
          //   name: "Reprezentacja wiedzy i systemy wnioskowania",
          //   description: "Reprezentacja wiedzy i systemy wnioskowania"
          // },
          {
            name: "Przetwarzanie języka naturalnego",
            description: "Przetwarzanie języka naturalnego"
          },
          {
            name: "Metody metaheurystyczne",
            description: "Metody metaheurystyczne"
          },
        ]
      },
      {
        spec_id: 3,
        name: "systemy informacyjne",
        description: "Informatyka to nie tylko programowanie. Dzisiaj pojedyncze programy komputerowe są częścią większych systemów informatycznych, podporządkowanych celom organizacji z nich korzystających.  Zanim powstanie program komputerowy musi on być zaprojektowany zgodnie z wymaganiami użytkowników – jest to zadanie dla analityków i projektantów, którzy posiadają nie tylko wiedzę techniczną, ale również rozumieją miejsce i rolę programu komputerowego w organizacji.  Celem specjalności „Systemy Informacyjne” jest przygotowanie studentów do aktywnego udziału w tworzeniu i eksploatacji nowych systemów informatycznych, poprzez dostarczenie im nie tylko wiedzy z zakresu informatyki, ale również wiedzy z zakresu zarządzania i ekonomii.",
        subjects: [
          {
            name: "Ochrona danych",
            description: "na tym przedmiocie mozemy poznac matematyke dyskretna"
          },
          {
            name: "Modelowanie procesów biznesowych",
            description: "na tym przedmiocie możemy poznać aplikacje spa"
          },
          {
            name: "Ład informatyczny",
            description: "Ład informatyczny"
          },
          {
            name: "Jakość oprogramowania",
            description: "Jakość oprogramowania"
          }
        ]
      }
    ],
  }
]
