/**
 * Created by cburas on 23.03.2017.
 */
export default [
  {
    id: 15,
    name: "Hurtownie danych",
    description: "Hurtownie danych"
  },
  {
    id: 16,
    name: "Inżynieria oprogramowania",
    description: "Inżynieria oprogramowania"
  },
  {
    id: 17,
    name: "Matematyka dyskretna",
    description: "Matematyka dyskretna"
  },
  {
    id: 18,
    name: "Zarządzanie procesowe",
    description: "Zarządzanie procesowe"
  },
  {
    id: 19,
    name: "Bogate aplikacje internetowe ",
    description: "Bogate aplikacje internetowe "
  },
  {
    id: 20,
    name: "Marketing i handel elektroniczny",
    description: "Marketing i handel elektroniczny"
  },
  {
    id: 21,
    name: "Metody optymalizacji procesów",
    description: "Metody optymalizacji procesów"
  },
  {
    id: 22,
    name: "Modelowanie i symulacja komputerowa",
    description: "Modelowanie i symulacja komputerowa"
  },
  {
    id: 23,
    name: "Programowanie kreatywne",
    description: "Programowanie kreatywne"
  },
  {
    id: 24,
    name: "Zwinne metody wytwarzania oprogramowania",
    description: "Zwinne metody wytwarzania oprogramowania"
  },
  {
    id: 25,
    name: "Wirtualizacja i przetwarzanie w chmurze ",
    description: "Wirtualizacja i przetwarzanie w chmurze"
  },
  {
    id: 26,
    name: "Zarządzanie strategiczne",
    description: "Zarządzanie strategiczne"
  },
  {
    id: 27,
    name: "Grafika komputerowa",
    description: "Grafika komputerowa"
  },
  {
    id: 27,
    name: "Modele obliczeniowe w ekonomii i finansach",
    description: "Modele obliczeniowe w ekonomii i finansach"
  }
]
