/**
 * Created by cburas on 23.03.2017.
 */
export default [
  {
    id: 1,
    name: "Teoretyczne podstawy informatyki",
    description: "Teoretyczne podstawy informatyki"
  },
  {
    id: 2,
    name: "Architektura systemów komputerowych",
    description: "Architektura systemów komputerowych"
  },
  {
    id: 3,
    name: "Elektroniczna wymiana danych",
    description: "Elektroniczna wymiana danych"
  },
  {
    id: 4,
    name: "Pracownia programowania II",
    description: "Pracownia programowania II"
  },
  {
    id: 5,
    name: "Wstęp do systemów informacyjnych",
    description: "Wstęp do systemów informacyjnych"
  },
  {
    id: 6,
    name: "Analiza i projektowanie aplikacji ",
    description: "Analiza i projektowanie aplikacji "
  },
  {
    id: 7,
    name: "Systemy operacyjne",
    description: "Systemy operacyjne"
  },
  {
    id: 8,
    name: "Pracownia programowania III",
    description: "Pracownia programowania III"
  },
  {
    id: 9,
    name: "Analiza i projektowanie systemów informacyjnych",
    description: "Analiza i projektowanie systemów informacyjnych"
  },
  {
    id: 10,
    name: "Wprowadzenie do baz danych",
    description: "Wprowadzenie do baz danych"
  },
  {
    id: 11,
    name: "e-Busines ",
    description: "e-Busines "
  },
  {
    id: 12,
    name: "Pracownia programowania IV ",
    description: "Pracownia programowania IV "
  },
  {
    id: 13,
    name: "Sieci komputerowe",
    description: "Sieci komputerowe"
  },
  {
    id: 14,
    name: "Pracownia programowania V",
    description: "Systemy operacyjne"
  }
]
