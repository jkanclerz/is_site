/**
 * Created by cburas on 23.03.2017.
 */
export default [
  {
    id: 1,
    name: "Organizacja i zarządzanie",
    description: "organizacja i zarządzanie"
  },
  {
    id: 2,
    name: "Wprowadzenie matematyka",
    description: "Wprowadzenie matematyka"
  },
  {
    id: 3,
    name: "Analiza matematyczna i algebra liniowa",
    description: "Analiza matematyczna i algebra liniowa"
  },
  {
    id: 4,
    name: "Rachunek prawdopodobieństwa i statystyka",
    description: "Rachunek prawdopodobieństwa i statystyka"
  },
  {
    id: 5,
    name: "Ekonomia",
    description: "Ekonomia"
  },
  {
    id: 6,
    name: "Metody numeryczne",
    description: "Metody numeryczne"
  },
  {
    id: 7,
    name: "Ekonomika i finanse przedsiębiorstw",
    description: "Ekonomika i finanse przedsiębiorstw"
  },
  {
    id: 8,
    name: "Prawo gospodarcze",
    description: "Prawo gospodarcze"
  },
  {
    id: 9,
    name: "Rachunkowość",
    description: "Rachunkowość"
  },
  {
    id: 10,
    name: "BHP i ergonomia ",
    description: "Pracownia programowania III"
  },
  {
    id: 11,
    name: "Socjologia",
    description: "Socjologia"
  },
  {
    id: 12,
    name: "Psychologia",
    description: "Psychologia"
  },
  {
    id: 13,
    name: "Pedagogika",
    description: "Pedagogika"
  },
  {
    id: 14,
    name: "Język angielski",
    description: "Język angielski"
  },
  {
    id: 15,
    name: "Matematyka dyskretna",
    description: "Matematyka dyskretna"
  },
  {
    id: 16,
    name: "Zarządzanie strategiczne",
    description: "Zarządzanie strategiczne"
  },
  {
    id: 17,
    name: "Wychowanie fizyczne",
    description: "Wychowanie fizyczne"
  },
  {
    id: 18,
    name: "",
    description: ""
  }
];
