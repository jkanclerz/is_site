import { IsSitePage } from './app.po';

describe('is-site App', function() {
  let page: IsSitePage;

  beforeEach(() => {
    page = new IsSitePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
